<?php

/**
 * La classe RobotLivreurPizza décrit le comportement du tout nouveau robot livreur de pizzas de JBHuet.com !
 *
 * Le nouveau robot livreur de pizzas de JBHuet.com est une formidable machine qui va chercher pour vous la pizza qui sort du four :
 * votre pizza préférée vient à vous, sans que vous ayez à bouger.
 * En pleine session de codage intense, pas possible de vous lever pour récupérer la pizza alors que le four sonne ? Pas de souci !
 * Le robot livreur de pizzas de JBHuet.com vous l'apporte sans effort, ou presque.
 * Cette merveilleuse machine se programme très simplement grâce à sa classe qui décrit très précisément tout ce qu'il peut faire pour vous.
 * Il vous suffit d'utiliser cette classe pour décrire à votre robot comment vous apporter votre délicieuse pizza.
 *
 * Ce robot perfectionné est doté d'un écran pour afficher les informations dont vous pourriez avoir besoin.
 * Vous pouvez même lui faire vous souhaiter un bon appétit !
 * NB. : ce robot étant encore à l'état de prototype, l'écran fournit actuellement ne peut afficher que des messages de 255 caractères maximum.
 * Grâce à ses capteurs (testeur de pente dans le sens du déplacement, testeur de distance aux obstacles face à lui, testeur de position de la pizza par rapport au plateau)
 * le robot peut s'adapter à de nombreuses situations.
 *
 * @author Jean-Bernard HUET <contact@jbhuet.com>
 *
 * @version 1.0.0
 *
 * PS. : les robots livreurs de pizzas existent déjà https://www.youtube.com/watch?v=rb0nxQyv7RU et https://www.youtube.com/watch?v=mIwDhnPnb4o
 */
class RobotLivreurPizza {
    //Attributs publics
    public $NomDuDev;
    // Attributs privés
    private $Obstacle;
    private $Distance;
    private $Pivoter;
    private $Plateau;
    private $pince;
    private $Spatule;


    /** @var string $Message Le message affiché à l'écran. */
    private $MessageEcran = "";

    /** @var integer CAPA_AFFICH Constante représentant le nombre maximal de caractères affichés à l'écran */
    private const CAPA_AFFICH = 255;

    /**
     * Titre de l'aide en ligne. Exemple : Création d'un objet de type RobotLivreurPizza
     *
     * Description de l'aide en ligne. Exemple : À la création d'un objet de type RobotLivreurPizza...
     *
     * Renseigner la ligne suivante s'il y a un paramètre. Ajouter autant de ligne d'information que de paramètre de la fonction.
     * @param <Type du paramètre> <nom de la variable : $...> <Description du paramètre : à quoi sert-il, quelle(s) valeur(s) lui donner, etc. ?>
     */
    public function __construct(string $Nom){
        $this->NomDuDev = $Nom;
    }
   
    // Méthodes publiques

    /**
     * Renseigner l'aide en ligne...
     * @param float $Distance Pour le méthodes Avancer() le chiffre devra être supérieur à 0,
     * @param bool $Obstacle = 0  valeur booléenne true pour présence d'un obstacle
     */
    public function Avancer( float $Distance, bool $Obstacle )  {
        if ($Obstacle == false
    ) {
            if (0 < $Distance) {
               $this->AfficherMessage(printf('J\'avance de %f mètres.</br>', $Distance));// Le robot affiche la distance sur laquelle il avance
        }else {
            $Distance = 0;
            $this->AfficherMessage (printf('Merci de renseigner une valeur supérieur à 0%f</br>', $Distance));
        }
     }
    }

    /**
     * Renseigner l'aide en ligne...
     * @param float $Distance Pour le méthodes Reculer() le chiffre devra être inférieur à 0,
     * @param bool $Obstacle = 0 valeur booléenne true pour présence d'un obstacle
     */
    public function Reculer( float $Distance, bool $Obstacle )  {
        if ($Obstacle == false
    )   {
            if (0 > $Distance) {
             $this->AfficherMessage(printf('Je Recule de %f mètres.</br>', $Distance));// Le robot affiche la distance sur laquelle il recule
          }else {
              $Distance = 0;
              $this->AfficherMessage (printf('Merci de renseigner une valeur inférieur à 0%f', $Distance));
          }
        }
    }
    
    

    /**
     *  Methode! function TournerDroite()
     * Méthode pour faire tourner à droite le robot
     * @param int $Pivoter le chiffre doit être compris entre 0.1 et 180 positif pour TournerDroite
     * celui-ci correspond à l'angle de Pivot du robot
     * 
     * @param $Obstacle=0 valeur booléenne true pour présence d'un obstacle
     */
    
    public function TournerDroite(float $Pivoter, bool $Obstacle)
    {
        if ($Obstacle == false
        ) {
            if (0 < $Pivoter && 180 >= $Pivoter) {
                $this->Pivoter = $Pivoter;
                $this->AfficherMessage(printf('Je tourne à droite de %f  °.</br>', $Pivoter));// Le robot affiche le nombre de degrés vers la droite dont il tourne sur lui-même
            }  else {
                $Rotation = 0;
                $this->AfficherMessage(printf('Mauvais angle de Pivot renseigné %f</br>', $Pivoter));
            }
        } 
    }

    /**
     * Methode! function TournerGauche()
     * Méthode pour faire tourner à gauche le robot
     * @param int $Pivoter le chiffre doit être compris entre -0.1 et -180 négatif pour TournerGauche
     * celui-ci correspond à l'angle de Pivot du robot
     * 
     * @param $Obstacle=0 valeur booléenne true pour présence d'un obstacle
     */
   
    public function TournerGauche(float $Pivoter, bool $Obstacle)
    {
        if ($Obstacle == false
        ) {
            if (0 < $Pivoter && -180 >= $Pivoter) {
                $this->Pivoter = $Pivoter;
                $this->AfficherMessage(printf('Je tourne à Gauche de %f  °.</br>', $Pivoter));// Le robot affiche le nombre de degrés vers la gauche dont il tourne sur lui-même
            }  else {
                $Rotation = 0;
                $this->AfficherMessage(printf('Mauvais angle de Pivot renseigné %f </br>', $Pivoter));
            }
        } 
    }

    /**
     *  Methode! function MonterPlateau()
     * Méthode pour faire monter et decendre le plateau
     * 
     * @param int $Plateau Le chiffre doit être compris entre 0 et 50 cm pour monter et -50 à 0 pour descendre,
     * info: le plateau est à 40 cm du sol et ne peut pas monter au dessus de 90cm d'ou valeur de mouvement ente 0 et 50 cm
     * 
     */
    
        # Le robot possède un plateau pour transporter la pizza.
        # Au plus bas, le plateau est à 40cm au-dessus du sol.
        # Le plateau est fixé sur un verrin qui peut monter ou descendre.
        # Selon les modèles de robot, le verrin peut monter plus ou moins haut.
        # La hauteur maximale du verrin (donc du plateau) est fixe (une constante).
        # Je vous laisse libre de déteriner cette hauteur maximale.
        # Il faudra vérifier que le robot ne reçoit pas un ordre de monter le plateau en dehors de la plage (hauteur minimum / hauteur maximum) possible.
        # Le robot affiche la hauteur en centimètres à laquelle monte le plateau.
    
    public function MouvementPlateau(float $Plateau)
    {
        if (0 <= $Plateau && 50 >= $Plateau) {
            $this->Plateau = $Plateau;
            $this->AfficherMessage(printf('Attention le plateau monte de %f cm</br>', $Plateau));
        } else if (0 >= $Plateau && -50 <= $Plateau) {
            $this->Plateau  = $Plateau;
            $this->AfficherMessage(printf('Attention le plateau descend de %f cm</br>', $Plateau * -1));
        } else if($Plateau = 0){
             $this->AfficherMessage(printf('Plateau ne bouge pas'));
        }
        else if (0 < $Plateau && 51 >= $Plateau) {
                 $this->Plateau = $Plateau;
                    $this->AfficherMessage(printf('Attention Mauvaise Mesure de monté renseigné 50 max %f</br>', $Plateau));
        } else if (0 > $Plateau && -51 <= $Plateau) {
                  $this->Plateau  = $Plateau;
                    $this->AfficherMessage(printf('Attention Mauvaise Mesure de décente renseigné -50max %f</br>', $Plateau));
              
        }
    }

    /**
     * Renseigner l'aide en ligne...
     */
    /**public function DescendrePlateau() {}
        /* Cette méthode est-elle utile ? */
        /* N'est-elle pas redondante avec MonterPLateau() ? */
        /* Vous pouvez la conserver, mais sans dupliuer le code de MonterPLateau() */
        /* ou bien renommer MonterPlateau() pour rendre le code plus lisible. */
    
    /**
     * Methode! function TirerPizzaSurPlateau()
     * Méthode pour sortie le la Pizza du four
     * 
     * @param int $pince valeur entre 0 et 40 
     * positif, une pince permet au robot d'attraper la pizza ,négative tire la pizza du four et glisser sur le plateau.
        * À côté du plateau, une pince permet au robot d'attraper la pizza pour la glisser sur le plateau.
         * Le robot affiche un message de confirmation que la pizza a bien était tirée et est maintenant sur le plateau.
         */
    
    public function TirerPizzaSurPlateau(float $pince)
    {
        if (0 <= $pince && 40 >= $pince) {
            $this->pince = $pince;
            $this->AfficherMessage(printf('Je vais chercher Pizza dans le Four %f</br>',$pince));
        } else if (0 >= $pince && -40 <= $pince) {
            $this->pince = $pince;
            $this->AfficherMessage(printf('Pizza sur le Plateau OK %f</br>',$pince));
        } else {
            $this->AfficherMessage(printf('Attention Mauvaise Mesure de monté renseigné 40 max %f</br>',$pince));
        }
    }

    /**
     * Methode! function PousserPizzaSurPlateau()
     * Méthode pour sortie le la Pizza du Plateau vers assiette
     * 
     * @param int $Spatule valeur entre 0 et 40 
     * positif, spatule coudée qui se glisse sous la pizza ,négative la pousser hors du plateau et glisser sur l'assiette.
     */
    /** */
    public function PousserPizzaSurPlateau($Spatule) {
        if (0 <= $Spatule && 40 >= $Spatule) {
            $this->Spatule = $Spatule;
            $this->AfficherMessage(printf('Je Prend Pizza sur plateau %f</br>',$Spatule));
        } else if (0 >= $Spatule && -40 <= $Spatule) {
            $this->Spatule = $Spatule;
            $this->AfficherMessage(printf('Pizza sur l\'assiette OK %f</br>',$Spatule));
            $this->AfficherMessage(printf('Bon Appetit %f !</br>', $this->NomDuDev));
        } else {
            $this->AfficherMessage(printf('Attention Mauvaise Mesure de monté renseigné 40 max %f</br>',$Spatule));
        }
        
    }

    /** Methode! function SetName()
     * Afin de personalisé le nom du mangeur de pizza
     *  
     * @param int $NouveauNom Valeur attendu String
     * 
     */
    public function SetName(int $NouveauNom)
    {
        $this->NomDuDev = $NouveauNom;
    }

    /**
     * Renseigner l'aide en ligne...
     */
    public function AfficherMessage( string $Message ) {
        $this->MessageEcran = $this->TesterLongueurMessage( $Message );
        print($this->MessageEcran );
    }

    // Méthodes privées

    /**
     * Vérifie que le message à afficher ne dépasse pas la capacité de l'écran
     *
     * La méthode privée TesterLongueurMessage teste si le message passé en paramètre dépasse la capacité d'affichage de l'écran.
     * Si le message dépasse la capacité de l'écran, tous les caractères au-delà de la capacité maximale de l'écran sont supprimés
     *    et "..." est ajouté à la fin du nouveau message.
     * NB. : la longueur maximale du nouveau message ("..." compris) ne peut pas dépasser la capacité d'affichage de l'écran.
     *
     * @link https://www.php.net/manual/fr/language.oop5.constants.php Pour comprendre la notation self::CAPA_AFFICH
     * @link https://www.php.net/manual/fr/function.strlen.php Pour savoir ce que fait la fonction PHP strlen
     *
     * @param string $MessagePossible Valeur du message avant réduction de la longueur si celle-ci dépasse la capacité de l'écran
     * @return string Valeur du message après réduction éventuelle de la longueur
     */
    private function TesterLongueurMessage( string $MessagePossible ): string {
        if (self::CAPA_AFFICH < strlen($MessagePossible)) {
            $MessagePossible = substr_replace($MessagePossible,'...',252);
        }

        return $MessagePossible;
    }

}

// Ecrivez ci-dessous le code qui sera transmis à votre robot pour aller chercher votre pizza dans le four et l'apporter à votre bureau
// En imaginant que vous possédez ce robot, et que vous êtes installé·e à votre bureau, programmez le robot pour qu'il vous rapporte votre pizza toute chaude.
// Donnez des ordres au robot en fonction de la réalité de votre logement.
// Le robot peut partir de n'importe quel point (sous votre bureau, un placard, un coin de votre cuisine).
// On considère que la porte du four est ouverte, et qu'elle n'empêche pas le robot d'atteindre la pizza.
// Le robot doit déposer la pizza dans une assiette posée au bord de votre bureau.
// La dernière instruction que le robot devra exécuter est de vous souhaiter un bon appétit.
$RobotPizzaNapolitain = new RobotLivreurPizza("Rv");
$RobotPizzaNapolitain -> Avancer(5, true);
$RobotPizzaNapolitain -> Avancer(2, false);
$RobotPizzaNapolitain ->TournerDroite(90, false);
$RobotPizzaNapolitain ->Avancer(2, false);

$RobotPizzaNapolitain->MouvementPlateau(30);
$RobotPizzaNapolitain->TirerPizzaSurPlateau(40);
$RobotPizzaNapolitain->TirerPizzaSurPlateau(-40);
$RobotPizzaNapolitain->MouvementPlateau(-30);

$RobotPizzaNapolitain -> Reculer(-2, false);
$RobotPizzaNapolitain->TournerDroite(180,false);
$RobotPizzaNapolitain -> Avancer(5, false);
$RobotPizzaNapolitain->TournerDroite(90, false);
$RobotPizzaNapolitain -> Avancer(5, false);
$RobotPizzaNapolitain->TournerDroite(90, false);
$RobotPizzaNapolitain -> Avancer(10, false);
$RobotPizzaNapolitain->TournerGauche(-90, false);
$RobotPizzaNapolitain -> Avancer(3, false);
$RobotPizzaNapolitain->TournerGauche(-90, false);
$RobotPizzaNapolitain -> Avancer(1, false);

$RobotPizzaNapolitain->MouvementPlateau(20);
$RobotPizzaNapolitain->PousserPizzaSurPlateau(40);
$RobotPizzaNapolitain->PousserPizzaSurPlateau(-40);
$RobotPizzaNapolitain->MouvementPlateau(-20);